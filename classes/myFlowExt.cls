/*Name: myFlowExt
 *Author: Jason Flammang (Vertiba)
 *Description : Apex extension for any VF pages using a Flow.  
                Used on the Contact page layout.
 * Methods:
		getFinishURL: gets the value of the varFinishURL from the flow.  If not set in the flow, default finish location is the record which
                      initiated the flow.  Otherwise, the finish URL set in the flow is used.
*/

public class myFlowExt {
    public Flow.Interview.My_Flow_API_Name1 flow { get; set; }
    public Flow.Interview.My_Flow_API_Name2 flow_2 { get; set; }
    public Flow.Interview.My_Flow_API_Name1 flow_3 { get; set; }
    public Contact con {get; set;}
    public string sObjectType;
    public Id defaultRecordId;
    public string flowFinishURL_Test;
    private string userType = UserInfo.getUserType();
    private boolean isPortalUser = true;
    private string pageURL = '';
    private string flowFinishPageURL = '';

    public myFlowExt(Apexpages.StandardController SC){
        if(userType != 'CspLitePortal'){
            isPortalUser = false;
        }
        sObjectType = SC.getRecord().getSObjectType().getDescribe().getName();
        if(sObjectType == 'Contact'){
            This.con = (Contact)SC.getRecord();
            defaultRecordId = con.Id;
            if(isPortalUser) pageURL = 'plbCommunity_Contact?Id=';
        }
        
    }
    
    public PageReference getFinishURL(){
        PageReference retVal;
        if(flow == null && flow_2 == null && flow_3 == null && flowFinishURL_Test == null){
            retVal = new PageReference('/' + pageURL + defaultRecordId);
            
        } else {
            string finishURL;
            if(flowFinishURL_Test != null) {
                finishURL = flowFinishURL_Test;
            } else {
                if(flow != null) {
                    finishURL = (String) flow.getVariableValue('varFinishURL');
                    if(isPortalUser) flowFinishPageURL = 'someOtherInternalPage?Id=';
                }
                if(flow_2 != null) {
                    finishURL = (String) flow_2.getVariableValue('varFinishURL');
                }
                if(flow_3 != null) {
                    finishURL = (String) flow_3.getVariableValue('varFinishURL');
                }
            }
            if (finishURL == '' || finishURL == null){
                retVal = new PageReference('/' + pageURL + defaultRecordId);
            } else {
                retVal = new PageReference('/' + flowFinishPageURL + finishURL);
            }
        }
            
        retVal.setRedirect(true);
        return retVal;
    }
}